/**
 * @file utils.h
 * This is the header file for Advent of Code 2022
 *
 * @author Eoghan Conlon
 *
 * @date
 * Created: 6/12/22
 * Last Modified: 6/12/22
 */

#ifndef INC_2022_C_UTILS_H
#define INC_2022_C_UTILS_H
///Standard Libraries
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>

///Structs & enums
typedef enum{
    ok,
    file_error
} error_codes;

typedef struct{
    FILE *file;
    error_codes errorCode;
}file_return;

typedef struct{
    error_codes errorCode;
    int output;
}output_return;

///Function Calls
file_return *open(char filePath[]);
void close(file_return *file);
output_return *day1_part1(FILE *file);

#endif //INC_2022_C_UTILS_H
