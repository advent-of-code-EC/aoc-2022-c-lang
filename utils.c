/**
 * @file utils.c
 * This is the utils file for Advent of Code 2022
 *
 * @author: Eoghan Conlon
 *
 * @dates
 * Created: 6/12/22
 * Last Modified: 6/12/22
 */

#include "utils.h"

file_return *open(char filePath[]){
    file_return *rValue = malloc(sizeof(file_return));
    if(rValue != NULL){
        rValue->errorCode = ok;
        rValue->file = fopen(filePath, "r");
        if(rValue->file == NULL){
            rValue->errorCode = file_error;
        }
    }
    return rValue;
}

void close(file_return *file){
    fclose(file->file);
    free(file);
}

output_return *day1_part1(FILE *file){
    int temp;
    int max = 0;
    int current = 0;
    char chunk[7];
    output_return *rValue = malloc(sizeof(output_return));
    if(rValue != NULL) {
        rValue->errorCode = ok;
        while (fgets(chunk, sizeof(chunk), file) != NULL) {
            if(chunk[0] != '\r'){
                temp = strtol(chunk, chunk, 10);
                current += temp;
            } else {
                if(max < current){
                    max = current;
                }
                current = 0;
            }
        }
        rValue->output = max;
    }
    return rValue;
}