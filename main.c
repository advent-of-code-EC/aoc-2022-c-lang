/**
 * @file main.c
 * This is the main file for Advent of Code 2022
 *
 * @author Eoghan Conlon
 *
 * @date
 * Created: 6/12/22
 * Last Modified: 6/12/22
 */

#include "utils.h"
/**
 *
 * @return
 */
int main(){
    file_return *file;
    output_return *result;
    char chunk[6];
    struct tm *local;
    time_t roughStart;
    time_t roughEnd;
    char day1[] = "./input/day01/input.txt";
    int day;
    int part;


    printf("Please input the day you want to work out: ");
    scanf("%d", &day);

    switch(day){
        case 1:{
            file = open(day1);
            break;
        }
        default:{
            perror("That day hasn't been programmed yet.");
            EXIT_FAILURE;
        }
    }
    if(file == NULL){
        perror("Failed to create return struct due to lack of memory");
        EXIT_FAILURE;
    }
    if(file->errorCode == file_error){
        perror("Failed to open file");
        EXIT_FAILURE;
    }
    printf("Please input the part you want to work out: ");
    scanf("%d", &part);
    time(&roughStart);  // read and record clock
    local = localtime(&roughStart);
    printf("# Start time and date: %s", asctime(local));
    switch(day){
        case 1:{
            if(part == 1){
                result = day1_part1(file->file);
            } else if(part == 2){
                printf("I have yet to program this part");
                close(file);
                EXIT_SUCCESS;
            } else {
                perror("There are only two parts per day");
                close(file);
                EXIT_FAILURE;
            }
            break;
        }
        default:{
            perror("You shouldn't be seeing this but stranger things have happened.");
            EXIT_FAILURE;
        }
    }
    if(result == NULL){
        perror("There was not enough memory to create the return struct.");
        close(file);
        EXIT_FAILURE;
    }
    printf("The output for part 1 is: %d\n", result->output);

    time(&roughEnd);
    local = localtime(&roughEnd);
    printf("# Start time and date: %s", asctime(local));

    close(file);
    EXIT_SUCCESS;
}
